from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()

class Autor(models.Model):
    nome = models.CharField(max_length=50)
    nascimento =  models.DateField('Data de Nascimento')
    usuarioi = models.OneToOneField(User,on_delete=None, related_name='autor')
    total_pub = models.IntegerField(default=0, blank=True)
    
    class Meta:
        verbose_name = 'Autor'
        verbose_name_plural = 'Autores'


class Artigo(models.Model):
    tema = models.SlugField(max_length=50)
    titulo = models.SlugField(max_length=50)
    conteudo = models.TextField()
    criado_em = models.DateTimeField("Criado em", auto_now_add=True)
    atualizado_em = models.DateTimeField("Criado em", auto_now=True)
    autor = models.ForeignKey(Autor, on_delete=None, related_name='artigo')

    class Meta:
        ordering = ['-criado_em']
        verbose_name = 'Artigo'
        verbose_name_plural = 'Artigos'