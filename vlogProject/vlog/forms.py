from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from vlog.models import Autor, Artigo

class AutorForm(forms.ModelForm):
    class Meta:
        model = Autor
        exclude = ['usuario']


class ArtigoForm(forms.ModelForm):
    class Meta:
        model = Artigo
        exclude = ['autor']