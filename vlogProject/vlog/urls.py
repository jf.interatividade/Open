from django import urls
from django.urls import path
from . import views

app_name = 'vlog'

urlpatterns = [
    path('home/', views.index, name='home'),
]