from django.shortcuts import render

def index(request):
    template_name = 'vlog/exemplo.html'
    return render(request, template_name)
